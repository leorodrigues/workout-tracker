# WORKOUT TRACKER

An application to track and analyse workout development over time.

## Installation

```elixir
def deps do
  [
    {:workout_tracker, "~> 0.0.1-a"}
  ]
end
```
