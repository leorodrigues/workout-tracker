defmodule WorkoutTracker do
  @moduledoc """
  Documentation for `WorkoutTracker`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> WorkoutTracker.hello()
      :world

  """
  def hello do
    :world
  end
end
