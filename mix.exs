defmodule WorkoutTracker.MixProject do
  use Mix.Project

  def project do
    [
      app: :workout_tracker,
      version: "0.0.1-a",
      elixir: "~> 1.12",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      aliases: aliases(),
      preferred_cli_env: [
        coveralls: :text,
        "coveralls.html": :test,
        integration: :test
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :runtime_tools],
      mod: {WorkoutTracker.Application, []}
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support", "test/mockery"]
  defp elixirc_paths(:dev), do: ["lib", "test/support", "test/mockery"]
  defp elixirc_paths(_), do: ["lib"]

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:meck, "~> 0.9.2", only: [:test], runtime: false},
      {:dialyxir, "~> 0.5", only: [:dev], runtime: false},
      {:excoveralls, "~> 0.10", only: [:test]},
      {:church, git: "http://bitbucket.org/leorodrigues/erlang-church.git", tag: "0.0.1-delta"},
      {:ex_test_support, git: "http://bitbucket.org/leorodrigues/ex-test-support.git", tag: "1.0.1"},
      {:scrolls, git: "http://bitbucket.org/leorodrigues/scrolls.git", tag: "1.0.0"},
      {:hermes, git: "http://bitbucket.org/leorodrigues/ex-hermes.git", tag: "1.0.0"}
    ]
  end

  defp aliases do
    [
      integration: ["test --only integration:true"]
    ]
  end
end
